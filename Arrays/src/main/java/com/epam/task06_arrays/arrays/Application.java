package com.epam.task06_arrays.arrays;

public class Application {
    public static void main(String[] args) {
        MyArray myArray = new MyArray();
        myArray.createArrayByMatches();
        myArray.createArrayByUnMatches();
        myArray.deleteDuplicateMoreThanTwo();
        myArray.deleteDuplicates();

        ArrayListAndArray arrayListAndArray = new ArrayListAndArray();
        arrayListAndArray.add100kStringsToArray();
        arrayListAndArray.add100kStringsToList();

        ComparableString comparableString = new ComparableString();
        comparableString.fillArrayList();
        System.out.println(comparableString.binarySearch("8"));
    }
}
