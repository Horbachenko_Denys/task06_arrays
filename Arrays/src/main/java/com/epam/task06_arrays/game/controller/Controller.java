package com.epam.task06_arrays.game.controller;

import com.epam.task06_arrays.game.model.Room;

public class Controller {
    Room room = new Room();

    public String getInfo() {
        return room.getInfo();
    }

    public int calculateMonsterDoors() {
        return room.calculateDeathDoors();
    }

    public String artifactDoorsArray(){
        return room.artifactDoors();
    }
}

