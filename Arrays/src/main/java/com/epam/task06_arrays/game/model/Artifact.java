package com.epam.task06_arrays.game.model;

import java.util.ArrayList;
import java.util.List;

public class Artifact extends HealthPoint {
    private static int summ = 0;
    private static List<Artifact> artifactDoor = new ArrayList<>();

    public Artifact() {
        super(80, 10);
        generateHp(10, 80);
        artifactDoor.add(this);
        summ += this.hp;
    }

    public static List<Artifact> getArtifactDoor() {
        return artifactDoor;
    }

    public static int getSumm() {
        return summ;
    }

    @Override
    public int activate() {
        if (isActivated()) return getHp();
        else return 0;
    }
}
