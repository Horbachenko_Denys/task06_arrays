package com.epam.task06_arrays.game.model;

import java.util.ArrayList;
import java.util.List;

public class Door {
    private static List<Door> doorList = new ArrayList<>();
    private static List<Integer> artifactOrder = new ArrayList<>();
    private static List<Integer> monsterOrder = new ArrayList<>();
    private static List<Integer> correctOrder = new ArrayList<>();
    private static int count = 1;
    private HealthPoint healthPoint;

    public Door() {
        if (Math.random() > 0.5) {
            healthPoint = new Artifact();
            artifactOrder.add(count);
            count++;
        } else {
            healthPoint = new Monster();
            monsterOrder.add(count);
            count++;
        }
        doorList.add(this);
    }

    public static List<Integer> getCorrectOrderOrder() {
        List<Integer> order = new ArrayList<>(artifactOrder);
        order.addAll(monsterOrder);
        return order;
    }

    public int open() {
        return healthPoint.activate();
    }

    @Override
    public String toString() {
        return String.format(" %10s\t\t %d",
                healthPoint.getClass().getSimpleName(), healthPoint.getHp());
    }


}