package com.epam.task06_arrays.game.model;


import java.util.Random;

public abstract class HealthPoint {
    private final int MAX_HP;
    private final int MIN_HP;
    protected int hp;
    private boolean activated;

    public HealthPoint(int maxHp, int minHp) {
        this.MAX_HP = maxHp;
        this.MIN_HP = minHp;
        this.hp = hp;
        this.activated = activated;
    }

    public abstract int activate();

    public int getHp() {
        return this.hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public int generateHp(int minHp, int maxHp) {
        Random rnd = new Random();
        return this.hp = rnd.nextInt(maxHp - minHp) + minHp;
    }
}
