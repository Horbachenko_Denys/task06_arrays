package com.epam.task06_arrays.game.model;

public class Hero {
    final static int START_HP_25 = 25;
    private int hp;

    Hero() {
        this.hp = START_HP_25;
    }

    public int changeHp(Door door) {
        hp += door.open();
        return hp;
    }

    public int getHp() {
        return hp;
    }
}
