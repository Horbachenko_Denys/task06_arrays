package com.epam.task06_arrays.game.model;

import java.util.ArrayList;
import java.util.List;

public class Monster extends HealthPoint {
    private static int summ = 0;
   private static List<Monster> monsterDoor = new ArrayList<>();

    public Monster() {
        super(100, 5);
        generateHp(5, 100);
        monsterDoor.add(this);
        summ += this.hp;
    }

    public static List<Monster> getMonsterDoor() {
        return monsterDoor;
    }

    public static int getSumm() {
        return summ;
    }

    @Override
    public int activate() {
        if (isActivated()) return getHp() * -1;
        else return 0;
    }
}
