package com.epam.task06_arrays.game.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Герой комп'ютерної гри, що володіє силою в 25 балів, знаходиться в круглому залі, з якого ведуть 10 закритих дверей.
 * За кожними дверима героя чекає або магічний артефакт, що дарує силу від 10 до 80 балів, або монстр, який має силу
 * від 5 до 100 балів, з яким герою потрібно битися. Битву виграє персонаж, що володіє найбільшою силою;
 * якщо сили рівні, перемагає герой.
 * 1. Організувати введення інформації про те, що знаходиться за дверима, або заповнити її,
 * використовуючи генератор випадкових чисел.
 * 2. Вивести цю саму інформацію на екран в зрозумілому табличному вигляді.
 * 3. Порахувати, за скількома дверима героя чекає смерть. Рекурсивно.
 * 4. Вивести номери дверей в тому порядку, в якому слід їх відкривати герою, щоб залишитися в живих, якщо таке можливо.
 */

public class Room {
    private static final int DOORS_COUNT_TEN = 10;
    List<Door> doors;
    Hero hero;
    private int summ = 0;


    public Room() {
        doors = new ArrayList<>();
        hero = new Hero();
        for (int i = 0; i < DOORS_COUNT_TEN; i++) {
            doors.add(new Door());
        }
    }

    public static int getDOORS_COUNT_TEN() {
        return DOORS_COUNT_TEN;
    }

    public String getInfo() {
        StringBuilder sb = new StringBuilder("\n   HP\n");

        for (Door d : doors) {
            sb.append(d.toString() + "\n");
        }
        return sb.toString();
    }

    public int calculateDeathDoors() {
        if(Monster.getMonsterDoor().size() == summ){
            return summ;
        }else {
            summ++;
            calculateDeathDoors();
        }return summ;
    }

    public List<Door> getDoors() {
        return doors;
    }

    public String artifactDoors() {
        if (Artifact.getSumm() - Monster.getSumm() + Hero.START_HP_25 >= 0) {
            return "You survive with next door order: " + Door.getCorrectOrderOrder() ;
        } else return "Sorry, you would not survive";
    }

}



