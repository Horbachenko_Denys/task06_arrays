package com.epam.task06_arrays.game.view;

import com.epam.task06_arrays.game.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyView {
    public static Logger logger = LogManager.getLogger(MyView.class);
    Controller controller = new Controller();

    public void start() {
        logger.info(controller.getInfo());
        showMonsterDoor();
        showArtifactDoor();
    }

    private void showArtifactDoor() {
        logger.info(controller.artifactDoorsArray());
    }

    private void showMonsterDoor() {
        logger.info("Doors where you die: " + controller.calculateMonsterDoors());
    }
}
