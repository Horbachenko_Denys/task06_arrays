package com.epam.task06_arrays.task;


import com.epam.task06_arrays.task.product.Laptop;
import com.epam.task06_arrays.task.product.Phone;
import com.epam.task06_arrays.task.product.Product;

import java.util.ArrayList;
import java.util.List;

public class Box<T> {
    public static void main(String[] args) {
        Phone phone = new Phone("nokia", 200, 100);
        Laptop laptop = new Laptop("toshiba", 500, 200);
        List<? super Product> src = new ArrayList<>();
        src.add(phone);
        src.add(laptop);
        for (Object o : src) {
            System.out.println(o);
        }
        List<? extends Product> dst = new ArrayList<>();
    }

}
